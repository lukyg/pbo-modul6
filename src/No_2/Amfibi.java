package No_2;

public class Amfibi {

    private String nama;
    private String caraBerkembangbiak;

    protected void wujud (String nama, String caraBerkembangbiak){
        this.nama = nama;
        this.caraBerkembangbiak = caraBerkembangbiak;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getCaraBerkembangbiak() {
        return caraBerkembangbiak;
    }

    public void setCaraBerkembangbiak(String caraBerkembangbiak) {
        this.caraBerkembangbiak = caraBerkembangbiak;
    }
}
