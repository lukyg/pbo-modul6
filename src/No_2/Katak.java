package No_2;

import java.util.Scanner;

public class Katak extends Amfibi{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Amfibi amfibi = new Amfibi();

        System.out.println("=== Testing Amfibi ===");
        System.out.print("Masukkan nama : ");
        amfibi.setNama(input.nextLine());
        System.out.print("Masukkan cara berkembang : ");
        amfibi.setCaraBerkembangbiak(input.nextLine());

        System.out.println("\n.... loading\n");

        System.out.print("Nama hewan adalah "+ amfibi.getNama());
        System.out.println(", dimana cara berkembang biaknya adalah "+
                amfibi.getCaraBerkembangbiak());
    }
}
