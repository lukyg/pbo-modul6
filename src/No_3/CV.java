package No_3;

public class CV extends Identitas{
    public static void main(String[] args) {
        CV person = new CV();
        person.setNama("Ade Safarudin madani");
        person.setNim("F1B019005");
        person.setUmur(21);
        person.setAlamat("Jln. Wisma Seruni");
        person.setRiwayatPendidikan("\n- SDN 23 Ampenan (2007-2009)" +
                "\n- SDN 7 Mataram (2009-2013)" +
                "\n- SMPN 2 Mataram (2013- 2016)" +
                "\n- SMAN 3 Mataram (2016-2019)");

        System.out.println("==== CV Ade Madani ====");
        System.out.println("Nama : "+person.getNama());
        System.out.println("NIM : "+person.getNim());
        System.out.println("Umur : "+person.getUmur());
        System.out.println("Alamat : "+person.getAlamat());
        System.out.println("Riwayat pendidikan : "+person.getRiwayatPendidikan());
    }
}
