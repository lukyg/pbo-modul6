package No_6;

abstract class KotaMataram {
    String namaKota = "Kota Mataram";
    protected abstract String nama();
    public String getNamaKota(){
        return namaKota;
    }
}

class Ampenan extends KotaMataram{
    String namaKec;
    protected String nama(){
        namaKec = "Ampenan";
        return namaKec;
    }
}

class Mataram extends KotaMataram{
    String namaKec;
    protected String nama(){
        namaKec = "Mataram";
        return namaKec;
    }
}

class AmpenanSelatan extends Ampenan{
    protected String namaKel = "Ampenan Selatan";
    void kelurahan(){
        System.out.println("Ini adalah "+getNamaKota()+" merupakan ibu kota NTB");
        System.out.println("Dimana salah satu kecamatannya yaitu "+nama());
        System.out.println("Salah satu kelurahan dari "+nama()+" yaitu "+namaKel);
    }
}

class KekalikJaya extends Mataram{
    protected String namaKel = "Kekalik Jaya";
    void kelurahan(){
        System.out.println("Ini adalah "+getNamaKota()+" merupakan ibu kota NTB");
        System.out.println("Dimana salah satu kecamatannya yaitu "+nama());
        System.out.println("Salah satu kelurahan dari "+nama()+" yaitu "+namaKel);
    }
}

class Jempong extends Mataram{
    protected String namaKel = "Jempong";
    void kelurahan(){
        System.out.println("Ini adalah "+getNamaKota()+" merupakan ibu kota NTB");
        System.out.println("Dimana salah satu kecamatannya yaitu "+nama());
        System.out.println("Salah satu kelurahan dari "+nama()+" yaitu "+namaKel);
    }
}

class Panggil{
    public static void main(String[] args) {
        AmpenanSelatan amp = new AmpenanSelatan();
        KekalikJaya klk = new KekalikJaya();
        Jempong jmp = new Jempong();

        amp.kelurahan();
        System.out.println(" ");
        klk.kelurahan();
        System.out.println(" ");
        jmp.kelurahan();
    }
}

